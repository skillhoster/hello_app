class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
  	#render plain: "Hello, World!"
  	render html: "Hello, World!"
  end

  def goodbye
  	#render plain: "Hello, World!"
  	render html: "Goodbye, World!"
  end
end
